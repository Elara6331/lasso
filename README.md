# Lasso

Lasso allows easily controlling multiple machines.

---

### About

Lasso consists of three parts. The lasso server, client, and lassoctl. The server keeps track of all the clients' local IPs, the clients update the local IP on the server when it changes, and lassoctl does the actual control.

This program uses a self-signed certificate, and is thus vulnerable to a MitM attack. If this occurs, it will only give the attacker access to the nodes' local IPs. All control goes through ssh, so it will not make nodes more vulnerable.

---

### Installation

The server should be installed on a centralized server that all the clients can access. To install the server, run:
```bash
sudo make install-server
```

The client should be installed on the computers which the server needs to keep track of. To install the server, run:
```bash
sudo make install-client
```

Lassoctl should be installed on the system from which commands will be issued. It can be installed like so:
```bash
sudo make install-lassoctl
```

---

### Configuration

The config for lasso is stored at `/etc/lasso/lasso.toml`.

For the server, the listen address, port, and location of TLS certificates can be changed.

For the client, the address and port of the server can be changed, as well as the name of the node.

The config for lassoctl is stored at `/etc/lassoctl.toml`.

The address and port of the server can be changed in the lassoctl config.

---

### Ports

The default port for lasso is TCP 11312. All control goes through SSH, which uses TCP 22. Make sure these ports are available or change them.

---

### Running

Running the server or the client can be done like so:

```bash
sudo systemctl start lasso
```

To enable the service on boot, run the following:

```bash
sudo systemctl enable lasso
```

---