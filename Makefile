PREFIX ?= $(DESTDIR)/usr/local
BINPREFIX ?= $(PREFIX)/bin
CFGPREFIX ?= $(DESTDIR)/etc/lasso
SERVICEPREFIX ?= $(PREFIX)/lib/systemd/system

all: lasso lassoctl
.PHONY: all

lasso:
	go build ./cmd/lasso

lassoctl:
	go build ./cmd/lassoctl

install-server: install-lasso gencert
	install -Dm644 server.service $(SERVICEPREFIX)/lasso.service
	install -Dm644 server.toml $(CFGPREFIX)/lasso.toml
.PHONY: install-server

install-client: install-lasso
	install -Dm644 client.service $(SERVICEPREFIX)/lasso.service
	install -Dm644 client.toml $(CFGPREFIX)/lasso.toml
.PHONY: install-client

install-lasso: lasso
	mkdir -p $(CFGPREFIX)
	install -Dm755 lasso $(BINPREFIX)/lasso
.PHONY: install-lasso

install-lassoctl: lassoctl
	install -Dm755 lassoctl $(BINPREFIX)/lassoctl
	install -Dm644 lassoctl.toml $(DESTDIR)/etc
.PHONY: install-lassoctl

gencert: lasso
	mkdir -p $(CFGPREFIX)/tls
	./lasso gencert $(CFGPREFIX)/tls/cert.pem $(CFGPREFIX)/tls/key.pem
.PHONY: gencert

clean:
	rm -f lasso lassoctl
.PHONY: clean