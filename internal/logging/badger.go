package logging

import (
	"strings"

	"github.com/rs/zerolog/log"
)

type BadgerLogger struct{}

func (BadgerLogger) Infof(format string, v ...interface{}) {
	log.Info().Msgf(strings.TrimSuffix(format, "\n"), v...)
}

func (BadgerLogger) Warningf(format string, v ...interface{}) {
	log.Error().Msgf(strings.TrimSuffix(format, "\n"), v...)
}

func (BadgerLogger) Debugf(format string, v ...interface{}) {
	log.Debug().Msgf(strings.TrimSuffix(format, "\n"), v...)
}

func (BadgerLogger) Errorf(format string, v ...interface{}) {
	log.Error().Msgf(strings.TrimSuffix(format, "\n"), v...)
}
