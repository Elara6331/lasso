package types

type Node struct {
	ID string `msgpack:"id,omitempty"`
	IP string `msgpack:"ip"`
}

type Response struct {
	Error   bool        `msgpack:"error"`
	Message string      `msgpack:"msg,omitempty"`
	Data    interface{} `msgpack:"data,omitempty"`
}
