/*
Copyright © 2021 Arsen Musayelyan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"os"
	"os/exec"
	"strings"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

// sshCmd represents the ssh command
var sshCmd = &cobra.Command{
	Use:   "ssh <user@node>",
	Short: "Make an ssh connection to the specified node",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			cmd.Usage()
			os.Exit(1)
		}

		// Get list of nodes from server
		nodes := getNodeList()

		// Split first argument by "@"
		splitArg := strings.Split(args[0], "@")

		// If split string has less than two elements, argument is invalid
		if len(splitArg) != 2 {
			log.Fatal().Msg("Invalid username/node argument")
		}

		// Get variables from split string
		username, nodeName := splitArg[0], splitArg[1]

		// Get node from list if it exists
		node, ok := nodes[nodeName]
		if !ok {
			log.Fatal().Str("node", nodeName).Msg("Node does not exist on the server")
		}

		// Create SSH command using node IP
		ssh := exec.Command("ssh", username+"@"+node.IP)
		ssh.Stdin = os.Stdin
		ssh.Stdout = os.Stdout
		ssh.Stderr = os.Stderr
		// Attempt to run command
		if err := ssh.Run(); err != nil {
			log.Fatal().Err(err).Msg("Error received from ssh command")
		}
	},
}

func init() {
	rootCmd.AddCommand(sshCmd)
}
